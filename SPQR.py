"""SPQR Parses Questionable Resources

SPQR is a quick and easy (dirty?) way to summarize SPARQL XML query 
results."""


__author__ = 'Michael Zeltner <http://niij.org/>'
__version__ = '0.2'
__license__ = 'Zope Public License (ZPL) Version 2.1'


from elementtree.ElementTree import XML


XML_NS = 'http://www.w3.org/XML/1998/namespace'
SPARQL_NS = 'http://www.w3.org/2001/sw/DataAccess/rf1/result2'
SPARQL_LITERAL = '{%s}literal' % SPARQL_NS
SPARQL_URI = '{%s}uri' % SPARQL_NS
SPARQL_BNODE = '{%s}bnode' % SPARQL_NS


class URI:
    """A URI."""
    def __init__(self, uri):
        self.uri = uri
    
    def __repr__(self):
        return "<%s>" % self.uri
    
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        elif other.uri == self.uri:
            return True
        else:
            return False
    

class Literal:
    """A literal."""
    def __init__(self, value, datatype=None, language=None):
        self.value = value
        self.datatype = datatype
        self.language = language
    
    def __repr__(self):
        if self.datatype == None and self.language == None:
            return '"%s"' % self.value
        if self.datatype == None:
            return '"%s"@%s' % (self.value, self.language)
        elif self.language == None:
            return '"%s"^^%s' % (self.value, self.datatype)
        else:
            return '"%s"' % self.value
    
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        elif other.value == self.value and \
             other.datatype == self.datatype and \
             other.language == self.language:
            return True
        else:
            return False
    

class BNode:
    """A blind node."""
    def __init__(self, id):
        self.id = id

    def __repr__(self):
        return "_:%s" % self.id

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        elif other.id == self.id:
            return True
        else:
            return False
    
def obtain_result_list(results):
    doc, results, bound_vars = XML(results), [], []
    
    vars = doc.findall('{%s}head/{%s}variable' % (SPARQL_NS, SPARQL_NS))
    
    for bound_var in vars:
        bound_vars.append(bound_var.items()[0][1])
        
    result = doc.findall('{%s}results/{%s}result' % (SPARQL_NS, SPARQL_NS))
    
    for bindings in list(result):
        data = {}
        for binding in list(bindings):
            bound_var = binding.items()[0][1]
            if not bound_var in bound_vars:
                raise KeyError("Not a bound variable: " + bound_var)
            for child in list(binding):
                data[bound_var] = str(_type_child(child))
        results.append(data)
    
    return results


def obtain_results(results):
    """Obtain"""
    doc, results = XML(results), {}
    
    bound_vars = doc.findall('{%s}head/{%s}variable' % (SPARQL_NS, SPARQL_NS))

    for bound_var in bound_vars:
        results[bound_var.items()[0][1]] = None

    bindings = doc.findall('{%s}results/{%s}result/{%s}binding'
                            % (SPARQL_NS, SPARQL_NS, SPARQL_NS))
    for binding in bindings:
        bound_var = binding.items()[0][1]
        b_child = list(binding)[0]

        if bound_var in results.keys():
            if results[bound_var] == None:
                results[bound_var] = _type_child(b_child)
            elif isinstance(results[bound_var], list) and \
                 not _type_child(b_child) in results[bound_var]:
                results[bound_var].append(_type_child(b_child))
            elif not isinstance(results[bound_var], list) and \
                 not _type_child(b_child) == results[bound_var]:
                results[bound_var] = [results[bound_var]]
                results[bound_var].append(_type_child(b_child))
        else:
            raise KeyError("Not a bound variable: " + bound_var)

    return results

def _type_child(b_child):
    """Return a typed and identified Node generated from the child of a
    binding element."""
    result = None
    if b_child.tag == SPARQL_LITERAL:
        datatype, language, text = None, None, ''
        
        if len(b_child.items()) > 0 and \
           b_child.items()[0][0] == 'datatype':
            datatype = URI(b_child.items()[0][1])
            result = Literal(b_child.text, datatype=datatype)
        elif len(b_child.items()) > 0 and \
             b_child.items()[0][0] == '{%s}lang' % XML_NS:
            language = b_child.items()[0][1]
        
        if b_child.text == None:
            text = ''
        else:
            text = b_child.text
            
        result = Literal(text, language=language, datatype=datatype)
    elif b_child.tag == SPARQL_URI:
        result = URI(b_child.text)
    elif b_child.tag == SPARQL_BNODE:
        result = BNode(b_child.text)
    return result
