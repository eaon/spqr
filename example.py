import RDF
import SPQR

storage = RDF.MemoryStorage("plazes")

model = RDF.Model(storage)
parser = RDF.Parser(name='rdfxml')

parser.parse_into_model(model, "http://aeim.niij.org/about/me.rdf")

class Namespaces:
    namespaces = {'foaf': 'http://xmlns.com/foaf/0.1/',
    'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
    'dc': 'http://purl.org/dc/elements/1.1/',
    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#'}
    
    human = namespaces.keys()
    machine = namespaces.values()
    
    def sparql(self):
        prefix = ""
        for key in self.human:
            prefix += "prefix %s: <%s>\n" \
                      % (key, self.machine[self.human.index(key)])
        return prefix
    
def query(query):
    return RDF.Query(Namespaces().sparql() + query, query_language="sparql")


foaf = query("""\
SELECT ?name ?nick ?depiction ?homepage
WHERE { ?document rdf:type foaf:PersonalProfileDocument;
           foaf:primaryTopic ?person; .
        ?person foaf:name ?name;
                foaf:nick ?nick;
                foaf:depiction ?depiction;
                foaf:homepage ?homepage; . }""")

crazy = query("""\
SELECT ?p ?o
WHERE { ?person foaf:name "Michael Zeltner";
        ?p ?o; . }""")

example_W3C = """<?xml version="1.0"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.w3.org/2001/sw/DataAccess/rf1/result2.xsd">

  <head>
    <variable name="x"/>
    <variable name="hpage"/>
    <variable name="name"/>
    <variable name="mbox"/>
    <variable name="age"/>
    <variable name="blurb"/>
    <variable name="friend"/>

    <link href="example.rq" />
  </head>

  <results ordered="true" distinct="false">

    <result>
      <binding name="x"><bnode>r1</bnode></binding>
      <binding name="hpage"><uri>http://work.example.org/alice/</uri></binding>
      <binding name="name"><literal>Alice</literal></binding>
      <binding name="mbox"><literal></literal></binding>
      <binding name="friend"><bnode>r2</bnode></binding>
      <binding name="blurb"><literal datatype="http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral">&lt;p xmlns="http://www.w3.org/1999/xhtml"&gt;My name is &lt;b&gt;alice&lt;/b&gt;&lt;/p&gt;</literal></binding>
    </result>

    <result> 
      <binding name="x"><bnode>r2</bnode></binding>
      <binding name="hpage"><uri>http://work.example.org/bob/</uri></binding>
      <binding name="name"><literal xml:lang="en">Bob</literal></binding>
      <binding name="mbox"><uri>mailto:bob@work.example.org</uri></binding>
      <binding name="age"><literal datatype="http://www.w3.org/2001/XMLSchema#integer">30</literal></binding>
      <binding name="friend"><bnode>r1</bnode></binding>
    </result>

  </results>

</sparql>"""

#SPQR.SPARQL_NS = 'http://www.w3.org/2005/sparql-results#'
print SPQR.obtain_result_list(str(crazy.execute(model)))
